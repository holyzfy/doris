<div class="footer">
    <div class="footer_bd">
        <a class="footer_qq" href="tencent://message/?uin=2197757189&Menu=yes" title="QQ"></a>
        <span class="footer_wechat" title="微信">
            <img width="300" src="<?php echo get_post_meta(get_option('page_on_front'), '微信二维码', true); ?>" alt="朵丽丝花艺设计">
        </span>
        <a class="footer_mail" href="mailto:2197757189@qq.com" title="邮箱"></a>
    </div>
</div>

<div style="width: 0; height: 0; overflow: hidden;">
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F7882356ce1a4d6c06a20a678c69ccd2c' type='text/javascript'%3E%3C/script%3E"));
</script>
</div>

<?php wp_footer(); ?>
</body>
</html>