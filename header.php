<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="//apps.bdimg.com/libs/jquery/1.11.3/jquery.min.js"></script>
<?php wp_head(); ?>
</head>
</head>

<body <?php body_class(); ?>>

<a class="logo" href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>

<?php if (has_nav_menu('primary')): ?>
    <?php
        wp_nav_menu(array(
            'theme_location' => 'primary',
            'container_class' => 'nav',
            'menu_id' => 'nav_bd',
            'menu_class' => 'nav_bd wrapper',
            'depth' => 2,
        ));
    ?>
<?php endif; ?>
