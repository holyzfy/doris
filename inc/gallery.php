<div id="gallery" class="wrapper gallery">
    <div id="gallery_grid_sizer" class="gallery_grid_sizer"></div>
    <div id="gallery_gutter_sizer" class="gallery_gutter_sizer"></div>
    <?php
        $attachments = array_map('get_attachment_image', wpba_get_attachments());
        foreach ($attachments as $value) {
            echo <<<GALLERY_ITEM
                <div class="gallery_item">
                    <img src="{$value->_medium[0]}" alt="" width="{$value->_medium[1]}" height="{$value->_medium[2]}">
                    <h3>$value->post_title</h3>
                    <p>$value->post_excerpt</p>
                </div>
GALLERY_ITEM;
        }
    ?>
</div>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/imagesloaded-4.1.1.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/masonry-4.1.1.js"></script>
<script>
var $gallery = $('#gallery').masonry({
    itemSelector: '.gallery_item',
    columnWidth: '#gallery_grid_sizer',
    gutter: '#gallery_gutter_sizer',
    percentPosition: true
});

$gallery.imagesLoaded().progress( function() {
    $gallery.masonry('layout');
});
</script>