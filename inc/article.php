<?php if(has_post_thumbnail()): ?> 
    <div class="wrapper cover"><?php the_post_thumbnail('large'); ?></div>
<?php endif;?>

<h1 class="title">
    <span><?php echo get_current_menu()->attr_title; ?></span>
    <em><?php echo get_the_title(); ?></em>
</h1>

<div class="wrapper content">
    <?php the_content(); ?>
</div>