<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css">
<script src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

<div id="slider" class="slider">
    <?php
        $attachments = array_map('get_attachment_image', wpba_get_attachments());
        foreach ($attachments as $value) {
            echo "<div><img src=\"{$value->_large[0]}\" alt=\"\"></div>";
        }
    ?>
</div>

<script>
<?php if(is_front_page() || is_home()): ?>
var options = {
    dots: true,
    arrows: false,
    adaptiveHeight: true,
    autoplay: true
};

<?php else: ?>

var options = {
    asNavFor: '#slider_nav',
    adaptiveHeight: true,
    prevArrow: '<button class="slider_arrow slider_prev" type="button">&lt;</button>',
    nextArrow: '<button class="slider_arrow slider_next" type="button">&gt;</button>'
};

<?php endif; ?>
$('#slider').slick($.extend(options, {
    respondTo: 'min'
}));
</script>

<?php if(!is_front_page() && !is_home()) : ?>
<div id="slider_nav" class="slider_nav">
    <?php
        $attachments = array_map('get_attachment_image', wpba_get_attachments());
        foreach ($attachments as $value) {
            echo "<div><img width=\"100\" src=\"{$value->_thumbnail[0]}\" alt=\"\"></div>";
        }
    ?>
</div>
<script>
$('#slider_nav').slick({
    respondTo: 'min',
    arrows: false,
    slidesToShow: 8,
    slidesToScroll: 1,
    asNavFor: '#slider',
    centerMode: true,
    focusOnSelect: true
});
</script>
<?php endif; ?>
