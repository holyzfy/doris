# Doris

Doris花艺网站

## 使用说明

0. 把此目录放到wordpress的`wp-content/themes`目录下，并启用该主题。
0. 安装插件

   - [File Renaming on upload](https://wordpress.org/plugins/file-renaming-on-upload/) ，配置如图 ![f.png](https://bitbucket.org/repo/np4A6e/images/4129495856-f.png)
   - [WP Better Attachments](https://wordpress.org/plugins/wp-better-attachments/)