<?php
/*
Template Name: 图文混排
*/
get_header();

while(have_posts()) {
    the_post();
    get_template_part('inc/article');
    get_template_part('inc/gallery');
}

get_footer();
?>