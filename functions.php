<?php
if ( ! function_exists( 'doris_setup' ) ) :
function doris_setup() {

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    add_theme_support( 'custom-logo', array(
        'height'      => 330,
        'width'       => 100,
        'flex-height' => true,
    ) );

    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'doris' )
    ) );

    add_editor_style('style/editor.css');
}
endif;
add_action('after_setup_theme', 'doris_setup');

function style() {
    wp_enqueue_style('doris', get_stylesheet_uri(), [], '11071753');
}
add_action('wp_enqueue_scripts', 'style');

function get_post_name($item_output, $item) {
    return empty($item->attr_title) ? $item_output : "<a href='$item->url'>$empty $item->attr_title</a>" . $item_output;
}
add_filter('walker_nav_menu_start_el', 'get_post_name', 10, 2);

function get_attachment_image($item) {
    $item->_thumbnail = wp_get_attachment_image_src($item->ID, 'thumbnail');
    $item->_medium = wp_get_attachment_image_src($item->ID, 'medium');
    $item->_large = wp_get_attachment_image_src($item->ID, 'large');
    return $item;
}

function get_current_menu() {
    global $post;
    $locs = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locs['primary']);
    if($menu) {
        $items = wp_get_nav_menu_items($menu->term_id);
        foreach ($items as $k => $v) {
            // Check if this menu item links to the current page
            if ($items[$k]->object_id == $post->ID) {
                $ret = $items[$k];
                break;
            }
        }
    }
    return $ret;
}

add_filter('body_class', function( $classes ) {
    global $post;
    return array_merge($classes, array('post_' . $post->post_name));
} );

?>
