<?php
/*
Template Name: 焦点图
*/
get_header();

while(have_posts()) {
    the_post();
    get_template_part('inc/article');
    get_template_part('inc/slider');
}

get_footer();
?>